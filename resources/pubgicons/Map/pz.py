import os
import PIL.ImageOps
from PIL import Image

with os.scandir('./') as fold:
   for ent in fold:
     if ent.name.endswith('py'):
         continue
     image = Image.open(ent.name)
     r, g, b, a = image.split()
     rgb_image = Image.merge('RGB', (r,g,b))
     inverted_image = PIL.ImageOps.invert(rgb_image)
     r2,g2,b2 = inverted_image.split()
     final_transparent_image = Image.merge('RGBA', (r2,g2,b2,a))
     final_transparent_image.save(ent.name)
