(() => {
    const Player = Vue.component('playertable', {
        props: ['users'],
        delimiters: ['[[', ']]'],

        data() {
            return {
                playerData: [],
                linkto: '',
                linkfrom: '',
            }
        },

        watch: {
            playerData: {
              handler(val) {
                this.$set(this, 'playerData', val);
              },
              deep: true,
            }
        },

        template: `
          <el-table 
            :data='playerData'
            :row-class-name='switchRowState'
            >
            <el-table-column prop='name' label='Name' width='240'>
            </el-table-column>
            <el-table-column prop='playerid' label='ID' width='380'>
            </el-table-column>
            <el-table-column label='Status'>
              <template slot-scope="scope">
                <template v-if='scope.row.is_banned'>
                  <el-tooltip content='このユーザは管理者によってBANされています。'>
                    <el-tag type='danger'>Banned</el-tag>
                  </el-tooltip>
                </template>
                <template v-else>
                  <el-tooltip content='このユーザはBANされていません。'>
                    <el-tag type='primary'>Fine</el-tag>
                  </el-tooltip>
                </template>
                <template v-if='scope.row.root !== null'>
                  <el-tooltip content='このアカウントは誰かの予備のものです。'>
                    <el-tag type='warning'>Alternative</el-tag>
                  </el-tooltip>
                </template>
                <template v-else>
                  <el-tooltip content='このアカウントは他のアカウントと関連がありません。'>
                    <el-tag type='success'>Root</el-tag>
                  </el-tooltip>
                </template>
              </template>
            </el-table-column>
            <el-table-column prop='created_date' width='190' label='Created At'>
            </el-table-column>
            <el-table-column
              class="operationBoundary"
              fixed="right"
              label="Operations"
              >
              <template slot-scope="scope">
                <el-popover placement="right" trigger="hover">
                  <ul class="uk-nav uk-nav-default">
                    <li><a @click='modifyDialog(scope.row)'>Modify</a></li>
                    <li><a @click='connectDialog(scope.row)'>Connect</a></li>
                    <li><a @click='banDialog(scope.row)'>Ban</a></li>
                  </ul>
                  <button class="uk-button uk-button-default" slot="reference">Actions</button>
                </el-popover>
              </template>
            </el-table-column>
          </el-table>
        `,

        mounted() {
            this.playerData = JSON.parse(this.users);
        },

        methods: {
            switchRowState({row, idx}) {
                const isSub = row.root !== null;
                const isBanned = row.is_banned;

                if(isBanned) {
                    return 'banned';
                }
                else if(isSub) {
                    return 'alt_account';
                }
                else {
                    return '';
                }
            },

            /* 
             * Show Modify Dialog and Do changing name operation.
             * @params user: user dictionary.
             * */
            modifyDialog(user) {
                // FIXME: make less concrete and strict.
                const dialogConfirmed = name =>  {
                    if (name === user.name || name === null) {
                        return null;
                    }

                    const action = { userid: user.playerid, action:'modify', name: name };
                    const response = this.sendRequest(action);
                    const callback = () => {
                        UIkit.notification('名前が正しく変更されました。', 'success');

                        // How the fuck this variable life cycle work?????
                        // FIXME: Make this less cursed and garbage for people who maintain this code
                        user.name = name;
                    };
                    
                    response.then(this.handleResponse(callback));
                }

                const Dialog = UIkit.modal.prompt('新しいユーザ名を入力してください。', user.name);
                const Data = Dialog.then(dialogConfirmed);
            },

            /*
             * Show Ban dialog and switch ban / un-ban state.
             * @ Params user: user directory.
             * */
            banDialog(user) {
                const confirmMsg = `${user.name}が${user.is_banned  ? 'BAN解除' : 'BAN'}されます。よろしいですか？`;
                const doneMsg = `${user.name}${user.is_banned ? 'のBANが解除' : 'がBAN'}されました。`; 
                
                const DialogConfirmed = () => {
                    const action = { userid: user.playerid, action: 'ban' };
                    const response = this.sendRequest(action);

                    const callback = () => {
                        UIkit.notification(doneMsg, 'success');

                        // FIXME: Make this less cursed and garbage for people who maintain this code
                        user.is_banned = !user.is_banned;
                    }

                    response.then(this.handleResponse(callback));
                }
                
                // You don't have to handle when it's rejected so I just pass an empty function
                UIkit.modal.confirm(confirmMsg).then(DialogConfirmed, () => {});
            },

            /*
             * Show connection dialog and connect / disconnect 2 accounts.
             * @ params user: user directory.
             * */
            connectDialog(user) {
            
            },

            /*
             * Function Wrapper for User related Response.
             * @params SuccessCallback: gets called when an operation is successfully done.
             * */
            handleResponse(SuccessCallback) {
                const reactToResponseData = response => {
                    const is_success = response.data.result === 'done';
                    if(is_success) {
                        SuccessCallback();
                    }
                    else {
                        UIkit.notification(response.data.errorText, 'danger'); 
                    }
                };

                return reactToResponseData;
            }, 

            /*
             * Fetches Cookie with given name. return undefined if it does not exist.
             * @params name: cookie name.
             * */
            getCookie(name) {
                const r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
                return r ? r[1] : undefined;
            },
            
            /*
             * Send an Action request with specific arguments.
             * @params action: Object consists of userid, action identifier, optional arguments.
             *
             * looks like this: 
             *  {
             *    userid: *User ID starts from 'account.' goes here.*,
             *    action: *Action Identifier String (1 of them: 'modify', 'connect', 'ban') goes here.*,
             *    name: *UNIQUE FOR 'modify': new name for given user.*
             *    linkto: *UNIQUE FOR 'connect': player id of root account.*
             *  }
             *
             *  @return Axios Promise.
             *  @TODO: Handle response inside of this. don't just separate handleResponse for no good reason.
             *  And Make this function's name more reasonable.
             * */
            sendRequest(action) {
                const url = window.location.origin + '/admin/users';
                const xsrf = this.getCookie('_xsrf');
                action._xsrf = xsrf;

                return axios(url, {
                    method: 'post',
                    params: action,
                    withCredentials: true,
                });
            },
        },
    });

    
    const Users = new Vue({
        el: '#useractions',
        delimiters: ['[[', ']]'],

        data() {
            return {
                users: [],
            }
        },

        methods: {
            getCookie(name) {
                const r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
                return r ? r[1] : undefined;
            },
            
            modifyUser(user) {
                const prompts = UIkit.modal.prompt('新しい名前', user.name)
                prompts.then(
                    // どうやらpromptsにはキャンセル時のハンドラがないらしい
                    // が、キャンセルを押した場合は必ずnullが引数に入るのでそこで判定する
                    (new_name) => {
                        // 変更がなかった場合は戻る
                        if(user.name === new_name || new_name === null) {
                            return
                        }

                        const action = { action:'modify', name: new_name };
                        const response = this.sendRequest(user, action);

                        response.then(resp => {
                            resp = resp.data;
                            const is_success = resp.result === 'done';
                            if(is_success) {
                              UIkit.notification('正常に名前が変更されました。', 'success');
                              user.name = new_name;
                            }
                            else {
                              UIkit.notification(resp.errorText, 'danger');
                            }
                        });
                    },
                );
            },

            connectUser(user, userlist) {
                console.log(user, userlist);
            },

            banUser(user) {
                console.log(user);
                const confirmer = UIkit.modal.confirm(`${user.name}が${user.is_banned  ? 'BAN解除' : 'BAN'}されます。よろしいですか？`);
                confirmer.then(
                    // confirmed
                    () => {
                        const action = { action: 'ban' };
                        const response = this.sendRequest(user, action);
                        
                        response.then(resp => {
                            resp = resp.data;
                            const is_success = resp.result === 'done';
                            const message = `${user.name}${user.is_banned ? 'のBANが解除' : 'がBAN'}されました。`;
                            
                            if (is_success) { 
                              UIkit.notification(message, 'success');
                              user.is_banned = !user.is_banned;
                            }
                            else {
                              UIkit.notification(resp.errorText, 'danger');
                            }
                        })
                    }
                );
            },


            sendRequest(user, action) {
                const url = window.location.origin + '/admin/users';
                const xsrf = this.getCookie('_xsrf');
                action.userid = user.playerid;
                action._xsrf = xsrf;

                return axios(url, {
                    method: 'post',
                    params: action,
                    withCredentials: true,
                });
            },
        }
    });
})();
