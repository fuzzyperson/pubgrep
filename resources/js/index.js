"use strict";
/**
 * I am so sorry for writing this code.
 * 
 * Maintainers, Embrace yourself.
 * be ready for next 6 months banging head to your desk.
 */

(function() {
    class TimeManager
    {
        constructor() {
            this.currentWeek = this.getWeekOfMonth();
            this.shift = 0;
        }

        ShiftRange(dir) {
            if(dir === 'right') this.shift += 1;
            if(dir === 'left') this.shift -= 1;
            if(dir === 'reset') this.shift = 0;
        }

        getWeekOfMonth() {
            return Math.ceil(moment().date() / 7);
        }

        // TODO: Anti-SOLID
        getRange(range = 'week')
        {
            if (!this.isAppropriateTime(range))
            {
                range = 'week';
            }

            let start = moment().add(this.shift, range).startOf(range);
            let end = moment().add(this.shift, range).endOf(range);

            if (range === 'week')
            {
                return { 'reqType': 'range', 'rangeType': range, 'start': start, 'end': end };
            }
            else {
                function genRange(start, end)
                {
                    const endOfRange = end.clone().endOf('month');
                    const ranges = [];
                    let count = 1;
                    let current = start.clone().startOf('month');

                    while(current.isSameOrBefore(endOfRange))
                    {
                        const end = current.clone().endOf('week');
                        const newItem = {'weeknum': count, 'start': current.unix(), 'end': end.unix()};
                        ranges.push(newItem);
        
                        count++;
                        current = current.add(7, 'days').startOf('week');
                    }

                    return ranges
                }

                let stringified;
                if (range === 'month')
                {
                    const ranges = genRange(start, end);
                    stringified = JSON.stringify(ranges);
                }

                else {
                    const monthTable = [];
                    const start_clone = start.clone();
                    let count = 1;
                    
                    while(start_clone.isSameOrBefore(end))
                    {
                        const endE = start_clone.clone().endOf('month');
                        const ranges = genRange(start_clone, endE);
                        monthTable.push({'monthnum': count, 'range': ranges});
                        count++;
                        start_clone.add(1, 'month').startOf('month');
                    }
                    stringified = JSON.stringify(monthTable);
                }
    
                return { 'reqType': 'range', 'rangeType': range, 'dateRange': stringified, 'start': start, 'end': end };
            }
        
        }

        isAppropriateTime(time)
        {
            return (time === 'week' || time === 'month' || time === 'year');
        }

        asUnixTime(time) {
            const timeUnix = Object.assign({}, time);
            timeUnix['start'] = timeUnix['start'].unix();
            timeUnix['end'] = timeUnix['end'].unix();

            return timeUnix;
        }

        asText(time) {
            let timeformat = 'YY-MM-DD';
            const range = time['rangeType'];
            console.log(range);

            if(range === 'week')
            {
                 timeformat = 'MM';
                 let startformat = time['start'].clone().format(timeformat);
                 let weeknum = Math.ceil(time['start'].clone().date() / 7);

                 return `${startformat}月 - ${weeknum} 週目`
            }
            else if(range === 'month') timeformat = 'YYYY-MM';
            else if(range === 'year') timeformat = 'YYYY';

            let start = time['start'].format(timeformat);

            return `${start}`;
        }
    };


    Vue.component('player-table', {
        props: ['match', 'dateRange'],
        delimiters: ['[[', ']]'],

        // The fact that vim does not indent the template correctly drives me nuts 
        // Don't say that I should use .Vue template, I'll do that later
        // Update: Switching to .vue + .ts in branch 'refactor'
        template: `
         <div>
           <div>
             <el-container class='uk-height-1-1 uk-height-match' uk-height-match='row: false'>  
               <el-aside width="28%" style="padding: 20px">
                 <div class=''>
                   <el-button-group>
                     <el-button icon="el-icon-arrow-left" @click="$emit('shift', 'left')" plain></el-button>
                     <el-button @click="$emit('shift', 'reset')">[[dateRange]]</el-button>
                     <el-button @click="$emit('shift', 'right')" plain><i class="el-icon-arrow-right el-icon-right"></i></el-button>
                   </el-button-group>
                 </div>
               </el-aside>
               <el-container>
                 <el-main>
                   <el-row :gutter="7" type="flex" class='uk-height-1-1'>
                     <template v-if='rangeType === "week"'>
                       <el-col :span='6' v-for='detail in details'>
                         <el-button class='uk-text-muted uk-width-1-1' @click="$emit('dialog', detail.uuid)" plain :disabled='detail.is_freezed'>
                           [[ detail['date'] ]]
                         </el-button>
                       </el-col>
                     </template>
                     <template v-else-if='rangeType === "month"'>
                       <el-col :span='4' v-for='detail in details'>
                         <el-button class='uk-text-muted uk-width-1-1' disabled>
                           [[ detail ]]
                         </el-button>
                       </el-col>
                     </template>
                   </el-row>
                 </el-main>
               </el-container>
             </el-container>
           </div>
           <div v-for='(player, key) in Players'>
             <el-card :body-style="{padding: '0px', height: '100%'}" class='uk-margin-bottom uk-height-small'>
               <el-container class='uk-height-1-1'>
                 <el-aside v-bind:class='playerNameClass(player)' width="28%">
                    <div class='uk-flex uk-height-1-1 uk-flex-center uk-flex-middle'>
                      <div class='uk-text-middle'  v-if='!player.is_banned'>
                        <div>
                          <span v-if='isRankedPlayer(player) || rangeType !== "week"'>[[ key+1 ]]位</span>
                          <span v-else> - 位</span>
                        </div>
                        <div class='uk-text-lead' v-bind:class='{"uk-text-muted": !isRankedPlayer(player)}'>
                         [[ player.name ]]
                        </div>
                        <span v-if='rangeType === "week"'>平均スコア [[player.mean]]</span>
                        <span v-else-if='rangeType === "month"'>合計スコア [[player.sum]]</span>
                        <span v-else>平均順位 [[player.rankmean]] 位</span>
                      </div>
                      <div class='uk-text-middle uk-flex-center' v-else>
                        <div class='uk-text-lead'>
                          BAN中
                        </div>
                      </div>
                    </div>
                 </el-aside>
                 <el-container>
                   <el-main v-if='rangeType !== "year"'>
                     <el-row :gutter="7" type="flex" v-if='!player.is_banned' class='uk-height-1-1'>
                       <el-col :span='rangeType==="week" ? 6 : 4' v-for='detail in details' class='uk-height-1-1'>
                         <div class='uk-height-1-1'>
                         <el-card 
                          class='uk-height-1-1 uk-flex uk-flex-center uk-flex-middle'
                          shadow='never'
                          :body-style='{ padding: "auto" }'
                          >
                             <span class='uk-text-muted'>
                             </span>
                             <div v-if='rangeType === "week"'>
                               <div v-if='detail.is_freezed'>
                                 <div class='uk-text-lead uk-text-muted'>
                                  除外中 
                                 </div>
                                 <span class='uk-text-muted'>管理者の判断で除外中</span>
                               </div>
                               <div v-else-if='player[detail.uuid] != 0'>    
                                 <div class='uk-text-lead'>
                                   [[ player[detail.uuid].score ]]
                                 </div>
                                 <div>
                                   <span class='uk-text-muted'> 
                                     [[ player[detail.uuid].place ]] 位 / [[ player[detail.uuid].kills ]] キル
                                   </span>
                                 </div>
                               </div>
                               <div v-else>
                               <div class='uk-text-lead uk-text-muted'>
                                 未参加
                               </div>
                               <span class='uk-text-muted'>
                                 データなし
                               </span>
                               </div>
                             </div>
                             <div v-else-if='rangeType === "month"'>
                               <div v-if="player[detail] !== 0">
                                 <div class='uk-text-lead' >
                                   [[ player[detail][1] ]]位
                                 </div>
                                 <div class='uk-text-muted'>
                                   [[ player[detail][0] ]]点
                                 </div>
                               </div>
                               <div v-else>
                                 <div class='uk-text-lead uk-text-muted'>
                                   未参加
                                 </div>
                               </div>
                             </div>
                         </el-card>
                         </div>
                       </el-col>
                     </el-row>
                     <el-row class='uk-height-1-1 uk-flex uk-flex-middle uk-flex-center' v-else>
                       <span class='uk-text-lead'>
                         BANNED
                       </span>
                     </el-row>
                   </el-main>
                   <el-main v-else>
                     <div class='uk-height-1-1'>
                       <line-chart class='uk-height-1-1' :dates='details' :scores='player.score' ></line-chart>
                     </div>
                   </el-main>
                 </el-container>
               </el-container>
             </el-card>
           </div>
         </div>
       `,

        data: function()
        {
            return {
                rangeType: 'week',
                details: [],
                summaries: [],
                loaded: false,
            }
        },

        mounted: function()
        {
            this.refreshData();
        },

        watch: {
            match ()
            {
                this.refreshData();
            },
        },

        computed: {
            Players ()
            {
                if (this.summaries === undefined ||
                     this.summaries === null)
                    {
                          return [];
                    }
                const players = this.summaries.filter(x => x['name']);
                return players;
            },
        },

        methods: {
            isRankedPlayer(player)
            {
                return player.played > 1;
            },

            playerNameClass(player)
            {
                if(player.is_banned) return {'aside-banned-player': true};

                if(this.isRankedPlayer(player) || this.rangeType !== 'week')
                {
                    return {
                        'aside-qualified-player': true,
                    };
                }
                else {
                    return {
                        'aside-insufficient-player': true,
                    };
                }

            },

            refreshData()
            {
                this.loaded = false;

                if(this.match.range === 'week')
                {
                    let detailSort = (a, b) => {
                        const ad = new Date(a['date']);
                        const bd = new Date(b['date']);

                        if (ad > bd) return 1;
                        if (ad < bd) return -1;
                        return 0;
                    }

                    this.$set(this, 'details', this.match.details.sort(detailSort));
                    this.$set(this, 'summaries', this.match.summaries);
                }
                else 
                {
                    this.$set(this, 'details', this.match.details);
                    this.$set(this, 'summaries', this.match.summaries);
                }
                this.$set(this, 'rangeType', this.match.range);
                this.loaded = true;
            }
        }

    })

    Vue.component('line-chart', {
        extends: VueChartJs.Line,
        props: ['dates', 'scores'],
        mounted: function() {
            const data = { 
                labels: this.dates,
                datasets: [
                    {
                        label: 'Scores',
                        pointRadius: 5,
                        borderColor: '#FEAAAA',
                        backgroundColor: '#FECCCC',
                        data: this.scores,
                        lineTension: 0,
                        showLine: 0,
                        spanGaps: true,

                    },
                ]}; 

            const options = {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            // reverse: true,
                        }
                    }]
                }
            }
            console.log(this.dates);
            console.log(this.scores);
            this.renderChart(data, options);
        }
    })

    new Vue ({

        el: '#rankapp',
        delimiters: ['[[', ']]'],

        data() {
            return {
                match: [],
                loaded: false,
                tableView: false,
                currentRange: 'week',
                currentRangeText: 'None',
                modalMatchData: {},
                modalShow: false,
                modalLoaded: false,
            }
        },

        /*
        Mount Function.
        */
        created: function () {
            this.TimeManager = new TimeManager();
            this.fetchMatchOf('week');
        },

        watch: {
            currentRange (v)
            {    
                let time = v;
                if(!this.TimeManager.isAppropriateTime(time))
                {
                    time = 'week';
                }
                this.TimeManager.ShiftRange('reset');
                this.fetchMatchOf(time);
                this.currentRange = time;
            }
        },

        methods: {
            async fetchMatchOf(time = 'week')
            {
                this.loaded = false;
                let range = this.TimeManager.getRange(time);
                let range_unix = this.TimeManager.asUnixTime(range);
                this.currentRangeText = this.TimeManager.asText(range);

                console.log(range_unix);
                try {
                    const match = await this._fetch_match(range_unix);
                    this.refreshView(match.data);
                    this.loaded = true;
                }
                
                catch (e) {
                    console.error(e);
                }
            },

            async showDialog(uuid)
            {
                const option = {'reqType': 'uuid', 'uuid': uuid};
                this.modalShow = true;
                this.modalLoaded = false;
                try {
                    const matches = await this._fetch_match(option);
                    const fullMatchData = matches.data.data;
                    this.$set(this, 'modalMatchData', fullMatchData);
                    this.modalLoaded = true;
                }

                catch(e) {
                    // TODO: implement error popup later.
                    // this is still better than just a console.log.
                    console.error(e);
                    this.modalLoaded = false;
                    this.modalShow = false;
                }
            },


            refreshView(data)
            {
                this.$set(this, 'match', data.data);
                this.switchView(data.range);
            },

            switchView(range)
            {
                if (range === 'year') {
                    this.tableView = false;
                    return;
                }
                this.tableView = true;
            },

            dateShift(direction) 
            {
                this.TimeManager.ShiftRange(direction);
                this.fetchMatchOf(this.currentRange);
            },

            // first underbar indicates these are private functions
            async _fetch_match(option) {
                console.log(option);
                const params = option;
                const url = 'http://' + window.location.host + '/api/match';

                return axios({url: url, params: params});
            },
        },
    })
})();
