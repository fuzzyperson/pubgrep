# -*- coding: utf-8 -*-

import os
import hashlib
import datetime

from tornado import web
from tornado import ioloop
from tornado import template

import pubgrep.config
from pubgrep import handler
from pubgrep.auth import PSWD
from pubgrep.database import init_database
from pubgrep.score import ScoreAdapter

conf = pubgrep.config.get_config()
score = ScoreAdapter(conf)

urls = [
          # For User
          (r"/", handler.StatsHandler, dict(config=conf)),
          (r"/login", handler.LoginHandler, dict(config=conf)),
          (r"/api/match", handler.MatchAPI, dict(config=conf, score=score)),

          # For Administrator
          (r"/admin", handler.AdminHandler, dict(config=conf)),
          (r"/admin/scores", handler.AdminScoreOperationHandler, dict(config=conf)),
          (r"/admin/replays", handler.AdminReplayHandler,dict(config=conf)),
          (r"/admin/users", handler.AdminUserHandler, dict(config=conf)),
          (r"/admin/matches", handler.AdminMatchesHandler, dict(config=conf)),
       ]

settings = {
        "cookie_secret": hashlib.sha512(os.urandom(32)).hexdigest(),
        "login_url": "/login",
        "xsrf_cookies": True,
        "static_path": os.path.join(os.path.dirname(__file__), 'resources'),
        "template_loader": template.Loader('./resources/templates'),
   }

def main():
    print(" PASSWORD ".center(64, "="), end='\n\n')
    print("管理者用パスワードが生成されました。")
    print("パスワードはサーバーを起動する毎に新しく生成されます。")
    print("情報の安全の為、パスワードは誰にも教えないでください。", end='\n\n')
    print("新しいパスワード:")
    print(PSWD, end='\n\n')
    print(" PASSWORD END ".center(64, "="))

    debug = conf.get('SETTINGS', 'debug')

    init_database(debug)

    if debug:
        print('デバッグモードがオンです。オートリロードが有効になりました。')

    try:
        webapp = web.Application(urls, **settings, debug=debug, autoreload=debug)
        webapp.listen(8888)
        ioloop.IOLoop.current().start()

    except KeyboardInterrupt:
        print('終了しています。')
        exit()

if __name__ == "__main__":
    main()
