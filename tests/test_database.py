# coding: utf-8

import pytest
import random

import peewee
import unittest.mock as mock
import playhouse.test_utils as pwtest

import pubgrep.database as pdb

from tests._util_test import TPrint
tprint = TPrint('MODEL')

db = peewee.SqliteDatabase(':memory:', pragmas={'foreign_keys': 1})
pdb.database.initialize(db)
pdb.database.create_tables([pdb.model.UserModel, pdb.model.MatchModel, pdb.model.KillModel, pdb.model.RankModel])

def _create_tempkey():
    return '{:011d}'.format(random.randint(1, int('9' * 11)))

def test_is_avoided():
    tprint('REGISTER_MATCH SHOULD AVOID UNSUCCESSFUL MODEL')
    model_unfinished = mock.MagicMock()
    model_unfinished.success = False

    assert not pdb.register_match(model_unfinished)


def test_is_match_rollbacked():
    tprint('REGISTER_MATCH SHOULD ROLLBACK WHEN GIVEN MATCH IS CORRUPTED')
    tempkey = _create_tempkey()
    model_corrupted = mock.MagicMock()
    model_corrupted.to_dict.return_value = {
        'uuid': tempkey
    }
    model_corrupted.success = True
    result, errorp = pdb.register_match(model_corrupted)
    assert not result
    assert errorp == 'MATCH'
    
    should_be_none = pdb.model.MatchModel.get_or_none(pdb.model.MatchModel.uuid == tempkey)
    assert should_be_none is None


def test_is_result_rollbacked():
    tprint('REGISTER_MATCH SHOULD ROLLBACK WHEN GIVEN RESULT IS CORRUPTED')
    import datetime
    tempkey = _create_tempkey()
    model_alright = mock.MagicMock()
    model_alright.success = True
    model_alright.to_dict.return_value = {
        'uuid': tempkey,
        'map': '',
        'perspective': '',
        # 'mode': self.matchtype,
        'duration': 0,
        'date': datetime.datetime.now(),
        'weather': 'clear',        
    }

    model_alright.result = { 'a': 'corrupted' }
    
    result, errorp = pdb.register_match(model_alright)
    assert not result
    assert errorp == 'RESULT'
    
    should_be_none = pdb.model.MatchModel.get_or_none(pdb.model.MatchModel.uuid == tempkey)
    assert should_be_none is None
