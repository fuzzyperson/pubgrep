class TPrint:
    def __init__(self, name):
        self.name = name

    def __call__(self, value):
        print("[{name}] {value}".format(name=self.name, value=value))
