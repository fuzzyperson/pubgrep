# coding: utf-8

import os
import sys
import pytest

import pubgrep.config as target

def test_config_created():
    target.get_config()
    assert os.path.isfile(target.CONFIG_PATH)

def test_config_has_section():
    conf = target.get_config()
    _ = conf['SETTINGS']['Debug']
    _ = conf['SCORE']['formula']
    _ = conf['SCORE']['bias']
    _ = conf['SCORE']['killscore']

