# coding: utf-8

import os
import json
import datetime

import requests
from pubgrep import model 
from pubgrep import network

def analyze(list_replayinfo):
    done = []
    failed = []
    for replayinfo in list_replayinfo:
        signature = extract_match_signature(replayinfo)

        # MatchInfo Format is Fixed.
        # Format goes like this:
        # match.bro.{mode}.{id?}.{platform}.{normal?}.{shard}.{year}.{month}.{day}.{hour}.{uuid}
        # We split this Matchinfo format with '.'(period).

        # Which information I need in order to request to the Official Server is:
        # Shard: Match region.
        # Year, Month, Day: match that's more than 14 days past from will get deleted from the server.
        # UUID: Match UUID.
        
        year, month, day = extract_date(signature)
        uuid = extract_uuid(signature)
       
        if is_datelimit_exceeded(year, month, day):
            failed.append('Error: Date Expired. {uuid}'.format(uuid=uuid))
            continue

        result = create_matchentity(uuid)

        if result.success:
            done.append(result)

        else:
            failed.append(result.error)


    return done, failed


def extract_match_signature(replayinfo):
    """ Return clopped un-neccessary bytes and jsoned data. """
    return json.loads(replayinfo[4:-1].decode('utf-8'))['FriendlyName'].split('.')

def extract_date(signature):
    return (i for i in map(int, signature[7:10]))

def extract_uuid(signature):
    return signature[-1]

def is_datelimit_exceeded(year, month, day):
    match_day = datetime.date(year=year, month=month, day=day)
    date_limit = datetime.date.today() - datetime.timedelta(days=14)
    
    # Date exceeded. 
    return match_day < date_limit


def parse_telemetry(telemetry) -> dict:
    parsed = {
        'LogPlayerAttack': {},
        'LogPlayerKill': [],
        'LogMatchEnd': None,
        'LogMatchStart': None,
        'GameResult': None
    }

    for packet in telemetry:
        packettype = packet.get('_T', False)
        if not packettype:
            continue

        if packettype == 'LogPlayerKill':
            parsed['LogPlayerKill'].append(packet)

        elif packettype == 'LogPlayerAttack':
            parsed['LogPlayerAttack'][packet['attackId']] = packet

        elif packettype == 'LogMatchEnd':
            parsed['LogMatchEnd'] = packet
            parsed['GameResult'] = packet['characters']

        elif packettype == 'LogMatchStart':
            parsed['LogMatchStart'] = packet

    if parsed['LogMatchStart'] is None or parsed['LogMatchEnd'] is None:
        return False

    return parsed

def create_matchentity(uuid):
    matchinfo = model.MatchEntity(uuid)
    url = network.MATCH_URL + uuid
    match = network.send_request(url)
    
    if match:
        telemetry = network.fetch_telemetry(match)

        if telemetry:
            parsed_telemetry = parse_telemetry(telemetry)
            telemetryresult = matchinfo.applyinfo(parsed_telemetry)

            if telemetryresult:
                matchinfo.success = True

            else:
                matchinfo.error = 'Match file is Corrupted.'

        else:
            matchinfo.error = 'Telemetry data Does not exist.'

    # TODO: What response is a 'bad response'? It could be 500, 501, 502...
    # Not knowing what type of response user got is bad. you should fix it.
    else:
        matchinfo.error = 'Bad Response.'

    return matchinfo

def structure_summary_response(details, summaries, score_func):
    """
    Parse Given SQL-ish Summary to more JS Friendly Parsed Structure.
    FIXME: I know that this is oddly specific,
           and goes against to the good coding practice.
           Help me.
    """
    freezed_match = set(map(lambda x: x['uuid'], filter(lambda x: x['is_freezed'], details)))
    user_table = { match['uuid']: 0 for match in filter(lambda x: not x['is_freezed'], details) }
    user_table.update(played=0, is_banned=False)
    players = {}

    for result in summaries:
        name, uuid = result['name'], result['uuid']
        score = score_func(result['place'], result['kills'])
        is_banned = result['is_banned']

        if name not in players:
            players[name] = user_table.copy()
            players[name]['total'] = []
            players[name]['total_place'] = []
            players[name]['total_kills'] = 0
            players[name]['name'] = name

        if uuid in freezed_match:
            score = 0
        
        players[name][uuid] = result
        players[name][uuid]['score'] = score

        players[name]['played'] += 1
        players[name]['is_banned'] = is_banned
        players[name]['total'].append(score)
        players[name]['total_place'].append(result['place'])
        players[name]['total_kills'] += result['kills']

    for player in players.values():

        # Since Every player in this match should have AT LEAST one played match,
        # This Mean Calculation will never spit out the 'ZeroDivision' Error
        player['sum'] = sum(player['total'])
        player['mean'] = sum(player['total']) // player['played']

    return list(players.values())
