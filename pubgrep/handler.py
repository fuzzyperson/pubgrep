# -*- coding: utf-8 -*-

from tornado import web
from tornado import ioloop
from tornado import template
from tornado import websocket
import datetime
import pprint

import os
import json
from pubgrep import auth, database, const, parse

class BaseHandler(web.RequestHandler):
    def initialize(self, config):
        self.config = config

    def get_current_user(self):
        return self.get_secure_cookie('token')


class StatsHandler(BaseHandler):
    def get(self):
        self.render('index.html')


class MatchAPI(BaseHandler):
    def initialize(self, config, score):
        super().initialize(config)
        self.score = score

    def get(self):
        request_type = self.get_argument('reqType')
        if request_type == 'range':
            request_range = self.get_argument('rangeType')
            arg_to_date = lambda x: datetime.datetime.fromtimestamp(int(x))

            if request_range == 'week':
                start = arg_to_date(self.get_argument('start'))
                end = arg_to_date(self.get_argument('end'))
      
                result_data = self.get_week_result(start, end)

            elif request_range == 'month':
                date_list =json.loads(self.get_argument('dateRange'))
                date_list.sort(key=lambda x: x['weeknum'])
                result_data = self.get_month_result(date_list)

            elif request_range == 'year':
                date_list =json.loads(self.get_argument('dateRange'))
                date_list.sort(key=lambda x: x['monthnum'])
                result_data = self.get_year_result(date_list)


        elif request_type == 'uuid':
            result_data = self.get_match_details()

        pprint.pprint(result_data)
        self.write({"data": result_data})

    # F**K
    def get_week_result(self, start, end):
        print(start, end)
        details, summaries = database.get_match_summary_between(start, end)
        summaries = parse.structure_summary_response(details, summaries, self.score)

        def sort_by_mean_and_qualified(x):
            qualified = 100000000 if x['played'] > 1 else 0
            ban_pushdown = -10000000000 if x['is_banned'] else 0
            return x['mean'] + qualified + ban_pushdown

        summaries.sort(key=sort_by_mean_and_qualified, reverse=True)
        return { 'range': 'week', 'details': details, 'summaries': summaries }

    # OH MY GOD IT HURTS
    def get_month_result(self, date_list):
        # 
        result_keys = ['{}週目'.format(date['weeknum']) for date in date_list]
        result_table = { key: 0 for key in result_keys }
        result = {}
        arg_to_date = lambda x: datetime.datetime.fromtimestamp(int(x))
        for date in date_list:
            week_number = '{}週目'.format(date['weeknum'])
            start, end = arg_to_date(date['start']), arg_to_date(date['end'])

            details, summaries = database.get_match_summary_between(start, end)
            print(details)
            summaries = parse.structure_summary_response(details, summaries, self.score)

            summaries_processed = list(filter(lambda x: x['played'] > 1 and not x['is_banned'], summaries))
            summaries_processed.sort(key=lambda x: x['mean'], reverse=True)

            for idx, player in enumerate(summaries_processed, start=1):
                name = player['name']
                if name not in result:
                    result[name] = result_table.copy()
                    result[name]['total'] = []
                    result[name]['total_place'] = []
                    result[name]['total_kills'] = 0
                    result[name]['name'] = name

                score = self.score(idx, 0)
                result[name][week_number] = [score, idx]
                result[name]['total'].append(score)
                result[name]['total_place'].extend(player['total_place'])
                result[name]['total_kills'] += player['total_kills']

        for player in result.values():
            player['sum'] = sum(player['total'])

        # Python 3.5 does not preserve the order of dictionary
        # key must be separately calculated with list
        # keys = list(result_table.keys())
        result = sorted(list(result.values()), key=lambda x: x['sum'], reverse=True)

        return { 'range': 'month','details': result_keys, 'summaries': result }


    # SMELL THE CODE
    def get_year_result(self, date_list):
        result_list = [None for date in date_list]
        result_name = ['{}月'.format(date['monthnum']) for date in date_list]
        result = {}

        for date in date_list:
            resultitem = self.get_month_result(date['range'])
            date_num = date['monthnum'] - 1
            for key, player in enumerate(resultitem['summaries'], start=1):
                name = player['name']
                if name not in result:
                    result[name] = {
                        'name': name,
                        'score': result_list[:],
                        'killmean': player['total_kills'] / len(player['total_place']),
                        'killsum': player['total_kills'],
                        'topcount': len(list(filter(lambda x: x == 1, player['total_place']))),
                        'rankmean': [],
                    }
                result[name]['rankmean'].extend(player['total_place'])
                result[name]['score'][date_num] = player['sum']
        
        for player in result.values():
            player['rankmean'] = round(sum(player['rankmean']) / len(player['rankmean']))
            player['sum'] = sum(filter(lambda x: x is not None, player['score']))

        result = sorted(list(result.values()), key=lambda x: x['rankmean'])

        return { 'range': 'year','details': result_name, 'summaries': result }



    def get_match_details(self):
        uuid = self.get_argument('uuid')
        match = database.get_match_details(uuid)

        if not match:
            match = {'error': 'UUID not found.'}

        else:
            # Match Detail Parse.
            match['mapimg'] = self.static_url("pubgicons/Map/{matchmap}.png".format(matchmap=match['map']))
            match['map'] = const.MapName[match['map']]

            match['weather'] = const.Weather[match['weather']]

            for kill in match['kills']:
                kill['killer'] = kill['killer']['player']['name']
                kill['victim'] = kill['victim']['player']['name']
                if kill['damagetype'] in const.WeaponDamage:
                    kill['killimg'] = self.static_url('pubgicons/Item/{}.png'.format(kill['weapon']))
                    kill['weapon'] = const.DamageCauserName[kill['damagecause']]

                    mods_unparsed = kill['weaponmods'].split(',') 
                    weaponmods = []
                    try:
                        for item in mods_unparsed:
                            weaponmods.append(const.Items[item])

                    except Exception as e:
                        print(e)
                        weaponmods = ['なし']

                    kill['weaponmods'] = weaponmods

                else:
                    kill['killimg'] = self.static_url('pubgicons/Killfeed/{}.png'.format(const.DamageCategory[kill['damagetype']]['icon']))
                    kill['weapon'] = const.DamageCategory[kill['damagetype']]['name']
                    kill['weaponmods'] = ['なし']

                kill['damagetype'] = const.DamageCategory[kill['damagetype']]['name']
                kill['damagepos'] = const.DamagePos[kill['damagepos']]
        
        return match


class AdminHandler(BaseHandler):
    @web.authenticated
    def get(self):
        self.render('admin.html')


class LoginHandler(BaseHandler):
    def get(self):
        if self.get_current_user():
            self.redirect('/admin')

        else:
            self.render('login.html')

    def post(self):
        try:
            _pswd = self.get_argument('password')

        except:
            self.redirect("/login")
            return

        if auth.check_password(_pswd):
            self.set_secure_cookie("token", auth.generate_token())
            self.redirect("/admin")

        else:
            self.redirect("/login")


class AdminReplayHandler(BaseHandler):
    def initialize(self, config):
        super().initialize(config)

    @web.authenticated
    def get(self):
        self.render('admin_replay.html')

    @web.authenticated
    def post(self):
        submitted_data = self.request.files.values()
        error = False
        submitted_files = []

        for submitted_file in submitted_data:
            submitted_file = submitted_file[0]['body']
            submitted_files.append(submitted_file)
            
        analyze_done, analyze_fail = parse.analyze(submitted_files)

        for replayinfo in analyze_done:
            uuid = replayinfo.uuid
            if database.is_match_exists(uuid):
                error = '{uuid}: This match is already Analyzed. Ignored.'.format(ignored=ignored)

            else:
                result = database.register_match(replayinfo)
                if not result:
                    error = 'replayinfo {uuid} failed: something went wrong.'.format(uuid=uuid)

        if error:
            self.write({'result': 'error', 'message': error})

        else:
            self.write({'result': 'done', 'message': '{uuid}: Successfully Applied.'.format(uuid=uuid)})


class AdminUserHandler(BaseHandler):
    USR_MODIFY = 'modify'
    USR_LINK = 'connect'
    USR_BAN = 'ban'

    @web.authenticated
    def get(self):
        users = list(database.get_all_users(asdict=True))
        for item in users:
            item['created_date'] = datetime.datetime.strftime(item['created_date'], '%Y-%m-%d') 

        self.render('admin_users.html', users=json.dumps(users))

    @web.authenticated
    def post(self):
        action = self.get_argument('action')
        steamid = self.get_argument('userid')

        result = False

        if action == self.USR_MODIFY:
            name = self.get_argument('name')
            result = database.modify_user(steamid, name)


        elif action == self.USR_LINK:
            link_to = self.get_argument('linkto')
            result = database.link_user(steamid, link_to)


        elif action == self.USR_BAN:
            result = database.ban_user(steamid)
            

        if not result:
            self.write({'result': 'error', 'action': action, 'errorText': 'error!'})

        else:
            self.write({'result': 'done', 'action': action, 'target': steamid})


class AdminScoreOperationHandler(BaseHandler):
    @web.authenticated
    def get(self):
        self.render("admin_scoreop.html")


class AdminMatchesHandler(BaseHandler):
    MTH_SHIFT = 'matchshift'
    MTH_FREEZE = 'matchfreeze'
    MTH_REMOVE = 'matchremove'

    @web.authenticated
    def get(self, uuid=None):
        match = database.get_all_match()
        self.render("admin_matches.html", matches=match, const=const)

    @web.authenticated
    def post(self, uuid=None):
        if uuid is None:
            uuid = self.get_argument('uuid')

        action = self.get_argument('action')
        result = False

        if action == self.MTH_SHIFT:
            shiftee = self.get_argument('shiftnum')
            result = database.shift_match(uuid, shiftee)

        elif action == self.MTH_FREEZE:
            result = database.freeze_match(uuid)

        elif action == self.MTH_REMOVE:
            result = database.remove_match(uuid)

        else:
            resu
                
        if not result:
            self.write({'result': 'error', 'action': action})

        else:
            self.write({'result': 'done', 'action': action, 'matchid': uuid})
