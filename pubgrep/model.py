# -*- coding: utf-8 -*-

"""
This API Provides a model / Entity functionality.
"""

import pprint
import datetime
import peewee
import playhouse.sqlite_ext as pwsqlite
from pubgrep.const import DBProxy, TIMEFORMAT, JST, UTC

class MatchEntity:
    def __init__(self, uuid):
        self.uuid = uuid
        
        self.success = False
        self._error = ''

        self.map = None
        self.perspective = None
        self.matchtype = None
        self.weather = None
        self.duration = 0
        self.timestamp = None

        self._result = {}
        self.kills = []

    @property
    def result(self):
        return list(self._result.values())

    @result.setter
    def result(self, _):
        raise NotImplementedError('This value is readOnly.')

    @property
    def error(self):
        error = '{UUID}: {Error}'.format(UUID=self.uuid, ERROR=self._error)
        return error

    @error.setter
    def error(self, value):
        self._error = value

    def to_dict(self):
        if not self.success:
            raise TypeError('This object is still processing.')

        date = datetime.datetime.fromtimestamp(self.timestamp).date()
        return {
            'uuid': self.uuid,
            'map': self.map,
            'perspective': self.perspective,
            # 'mode': self.matchtype,
            'duration': self.duration,
            'date': date,
            'weather': self.weather,
        }
    
    def applyinfo(self, telemetry):

        # ORDER MATTERS: Applykill uses Dictionary which will get made by applyresult
        matchapplied = self._applymatch(telemetry['LogMatchStart'], telemetry['LogMatchEnd'])
        resultapplied = self._applyresult(telemetry['GameResult'])
        killapplied = self._applykill(telemetry['LogPlayerKill'], telemetry['LogPlayerAttack'])

        # If one of them failed, That match will not registered
        if not killapplied or not resultapplied or not matchapplied:
            return False

        self.success = True
        return True


    def _applymatch(self, log_start, log_end):
        # Perspective and detail set
        try:
            self.perspective = log_start['cameraViewBehaviour']
            self.map = log_start['mapName']

            # just in some case where map does not have a weather system.
            self.weather = log_start.get('weatherId', 'Clear')
        
        except KeyError as e:
            self._error = e.args
            print('ApplyMatch Failed:', e)
            return False

        # Get Date and fix timezone
        startdate = datetime.datetime.strptime(log_start['_D'], TIMEFORMAT)
        enddate = datetime.datetime.strptime(log_end['_D'], TIMEFORMAT)
        
        startdate = startdate.replace(tzinfo=UTC).astimezone(JST)
        enddate = enddate.replace(tzinfo=UTC).astimezone(JST)
        
        self.timestamp = startdate.timestamp()
        self.duration = (enddate - startdate).seconds

        return True

    def _applykill(self, kills, damages):
        processedmodels = []
        for kill in kills:
            killinfo = KillEntity()
            try:
            # These two id are crucial, You cannot avoid it
                killinfo.killer = kill['killer']['accountId']
                killinfo.victim = kill['victim']['accountId']

                # If this kill is caused by bluezone / redzone / other non-player stuff
                if killinfo.killer == '':
                    killinfo.killer = killinfo.victim

            except KeyError:
                print('ApplyKill Error: Account ID Not found.')
                self._error = 'ApplyKill Error: Account ID Not Found'
                return False

            # Other data such as weapon is kinda optional,
            # It's not much of a problem if you couldn't catch it
            killinfo.damagecause = kill.get('damageCauserName', 'Death')
            killinfo.damagepos = kill.get('damageReason', 'NonSpecific')
            killinfo.damagetype = kill.get('damageTypeCategory', 'Damage_Groggy')

            killid = kill['attackId']
            if killid in damages:
                last_attack = damages[killid]

                if last_attack['attackType'] == 'Weapon':
                    killinfo.weapon = last_attack['weapon']['itemId']

            killinfo.weaponmods = kill.get('damageCauserAdditionalInfo', list())

            self._result[killinfo.killer].add_kill(killinfo)
            processedmodels.append(killinfo)

        self.kills = processedmodels
        return True

    def _applyresult(self, stats):
        stats.sort(key=lambda x: x['ranking'])

        for rank in stats:
            name = rank['name']
            accountid = rank['accountId']
            place = rank['ranking']

            result = ResultEntity(name, accountid, place)

            self._result[accountid] = result

        return True


class ResultEntity:
    def __init__(self, name, accountid, place):
        self.name = name
        self.accountid = accountid
        self.place = place
        self.kills = []

    def add_kill(self, killentity):
        self.kills.append(killentity)
        killentity.parent = self

    def to_dict(self):
        return {
            'name': self.name,
            'playerid': self.accountid,
            'place': self.place,
            'kills': self.kills,
        }


class KillEntity:
    killer = None
    victim = None
    weapon = 'Death'
    damagecause = 'Death'
    damagepos = 'NonSpecific'
    damagetype = 'Damage_Groggy'
    weaponmods = None
    
    def __repr__(self):
        return '<KillInfo>{ -[{}]-> {}'.format(self.killer, self.weapon, self.victim)

    def __init__(self):
        self.parent = None
        self.weaponmods = []

    def to_dict(self):
        if self.is_environment_kill():
            self.killer = self.victim

        return {
            'killer': self.killer,
            # 'killerresult': self.parent,
            'victim': self.victim,
            'weapon': self.weapon,
            'weaponmods': ','.join(self.weaponmods),
            'damagepos': self.damagepos,
            'damagetype': self.damagetype,
            'damagecause': self.damagecause
        }

    def is_suicide(self):
        # Kill event will record killer and victim even though it's suicide
        # Maybe because technically suicide is 'Killing themselves'
        return self.killer == self.victim

    def is_environment_kill(self):
        return self.killer == ''

class UserModel(peewee.Model):
    class Meta:
        database = DBProxy
        table_name = 'Users'

    playerid = peewee.CharField(unique=True)

    name = peewee.CharField()
    root = peewee.ForeignKeyField('self', null=True, backref='alt') 

    created_date = peewee.DateTimeField(default=datetime.datetime.now)
    is_banned = peewee.BooleanField(default=False)

class MatchModel(peewee.Model):
    class Meta:
        database = DBProxy
        table_name = 'Matches'

    numbering = peewee.IntegerField()
    uuid = peewee.CharField(unique=True)
    map = peewee.CharField()

    weather = peewee.CharField()
    perspective = peewee.CharField()

    date = peewee.DateField()
    duration = peewee.TimeField()
    is_freezed = peewee.BooleanField(default=False)

class RankModel(peewee.Model):
    class Meta:
        database = DBProxy
        table_name = 'Ranks'

    match = peewee.ForeignKeyField(MatchModel, backref='places')
    player = peewee.ForeignKeyField(UserModel, backref='+')
    place = peewee.IntegerField()


class KillModel(peewee.Model):
    class Meta:
        database = DBProxy
        table_name = 'Kills'
    
    match = peewee.ForeignKeyField(MatchModel, backref='kills')
    killer = peewee.ForeignKeyField(RankModel, backref='kills')
    victim = peewee.ForeignKeyField(RankModel, backref='+')
    weapon = peewee.CharField()
    damagecause = peewee.CharField()
    weaponmods = peewee.CharField(null=True)
    damagetype = peewee.CharField()
    damagepos = peewee.CharField()
