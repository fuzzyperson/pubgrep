# coding: utf-8

import os
import configparser

CONFIG_PATH = './config.ini'


def get_config():
    if is_config_file_exists():
        config = configparser.ConfigParser()
        config.read(CONFIG_PATH)

    else:
        config = create_default_config()

    return config


def create_default_config():
    config = configparser.ConfigParser()

    # Settings Section:
    #   Debug - if it's debug mode or not
    config.add_section('SETTINGS')
    config['SETTINGS']['Debug'] = 'yes'

    # Score Section:
    #   Formula: Formula for calculation
    #   Bias: Least Bias
    #   Killscore: Additional Score for each kill
    config.add_section('SCORE')
    config['SCORE']['formula'] = 'linear'
    config['SCORE']['bias'] = '0'
    config['SCORE']['killscore'] = '50'

    with open(CONFIG_PATH, 'w') as conf_f:
        config.write(conf_f)

    return config


def is_config_file_exists():
    return os.path.isfile(CONFIG_PATH)
