# coding: utf-8

"""
This script provides a database functionality.
"""

import peewee
import datetime
import pendulum
import playhouse.shortcuts as ph_shortcuts
import pprint

from pubgrep import model
from pubgrep.const import DBProxy, DBPROD, DBDEBUG
import playhouse as ph

from typing import (
    List, Callable, Dict, Union, Optional
)

def init_database(debug: bool) -> None:
    """ Initialize Database. """
    db = DBPROD
    if debug:
        # if os.path.isfile(DBDEBUG):
        #     os.remove(DBDEBUG)

        db = DBDEBUG

    db = ph.sqlite_ext.SqliteExtDatabase(db, pragmas=(
            ('cache_size', 1024 * 128),
            ('journal_mode', 'wal'),
            ('foreign_keys', 1),
        ))
    DBProxy.initialize(db)
    DBProxy.create_tables([model.UserModel, model.MatchModel, model.KillModel, model.RankModel])


# TODO: Kill / Rank creation can be Separated into other Function. Do it.
def register_match(matchinfo: model.MatchEntity) -> bool:
    if not matchinfo.success:
        return False

    match_data = matchinfo.to_dict()
    uuid = match_data['uuid']
    max_numbering = model.MatchModel.select(peewee.fn.MAX(model.MatchModel.numbering).alias('count'))[0]
    count = max_numbering.count
    if count is None:
        count = 1

    match_data['numbering'] = count + 1

    # preserve rank model after creation to link kill more easier afterwards.
    rank_model_holder = {}
    with DBProxy.atomic() as atomic:
        try:
            match = ph_shortcuts.dict_to_model(model.MatchModel, match_data)
            match.save()

        except Exception as e:
            print('error during :MATCH CREATION:', e)
            atomic.rollback()
            return False

        try:
            for result in matchinfo.result:
                player, created = model.UserModel.get_or_create(playerid=result.accountid,
                                                                defaults={'name': result.name})

                if player.name != result.name:
                    player.name = result.name
                    player.save(only=player.dirty_fields)

                rank = model.RankModel.create(match=match, player=player, place=result.place)
                rank_model_holder[result.accountid] = rank

        except Exception as e:
            print('Error During :USER / RANK CREATION:', e)
            atomic.rollback()
            return False

        try:
            for killentity in matchinfo.kills:
                kill_data = killentity.to_dict()
                kill_data['match'] = match
                kill_data['killer'] = rank_model_holder[kill_data['killer']]
                kill_data['victim'] = rank_model_holder[kill_data['victim']]

                kill = ph_shortcuts.dict_to_model(model.KillModel, kill_data)
                kill.save()

        except Exception as e:
            print('Error During :KILL CREATION:', e)
            atomic.rollback()
            return False

    return True

def shift_match(uuid, shiftnum):
    if type(shiftnum) == str:
        shiftnum = int(shiftnum)
        
    match = model.MatchModel.get_or_none(model.MatchModel.uuid == uuid)
    is_overlap = model.MatchModel.select(model.MatchModel.numbering == shiftnum).exists()
    if match is None:
        return False

    target = match.numbering
    shift_target = []
    shift_step = 0
    if shiftnum == target:
        return True

    elif target > shiftnum:
        shift_target = model.MatchModel.select().where(model.MatchModel.numbering.between(shiftnum, target-1))
        shift_step = 1

    elif target < shiftnum:
        shift_target = model.MatchModel.select().where(model.MatchModel.numbering.between(target+1, shiftnum))
        shift_step = -1

    with DBProxy.atomic() as atomic:
        try: 
            if is_overlap:
                for item in shift_target:
                    item.numbering = item.numbering + shift_step
                    item.save()

            match.numbering = shiftnum
            match.save()

        except Exception as e:
            atomic.rollback()
            print(e)
            return False

    return True

def lock_matches(date):
    """
    Lock All matches with given date.
    """


def remove_match(uuid):
    match = model.MatchModel.get_or_none(model.MatchModel.uuid == uuid)
    if match is None:
        return False

    # Order Matters: Kill has key to the rank
    model.KillModel.delete().where(model.KillModel.match == match).execute()
    model.RankModel.delete().where(model.RankModel.match == match).execute()
    match.delete_instance()
    return True

def get_match_details(uuid: str) -> dict:
    """Returns Detail of match with given uuid.

    Arguments:
        uuid {str} -- Match UUID.

    Returns:
        Dict -- Full Match Data.

    """
    Match, Kill, Rank, User = model.MatchModel, model.KillModel, model.RankModel, model.UserModel
    match = model.MatchModel.get_or_none(model.MatchModel.uuid == uuid)

    pprint.pprint(match)
    if match is None:
        return False

    excludes = [User.created_date, User.alt, Rank.kills]
    match = ph_shortcuts.model_to_dict(match, backrefs=True, exclude=excludes)
    match['date'] = datetime.datetime.strftime(match['date'], '%Y-%m-%d')
    return match


def get_match_summary_between(start: datetime.datetime, end: datetime.datetime) -> List:
    """Returns Summaries of match within given range.

    Arguments:
        start {int} -- Start Timestamp.
        end {int} -- End Timestamp.

    Returns:
        List -- Summaries of Match.
    """

    # Just to make it more small
    Match, Kill, Rank, User = model.MatchModel, model.KillModel, model.RankModel, model.UserModel
    matches = (Match.select(Match.uuid, Match.date, Match.is_freezed).where(Match.date.between(start, end)).dicts())

    summaries = []
    # Swap Date to String
    for i in matches:
        i['date'] = datetime.datetime.strftime(i['date'], '%Y-%m-%d')
        
        results = (Rank
                 .select(User.name, User.is_banned, Rank.place, Match.uuid, peewee.fn.count(Kill.id).alias('kills'))
                 .join_from(Rank, User)
                 .join_from(Rank, Match)
                 .join(Kill, peewee.JOIN.LEFT_OUTER, on=(Kill.killer == Rank.id))
                 .where(Match.uuid == i['uuid'])
                 .group_by(Rank)
                 .dicts())

        summaries.extend([*results])

    return [*matches], summaries

def freeze_match(uuid: str) -> bool:
    match = model.MatchModel.get_or_none(model.MatchModel.uuid == uuid)
    if match is None:
        return False

    match.is_freezed = not match.is_freezed
    match.save(only=match.dirty_fields)
    return True


def get_all_users(asdict: bool = False) -> Union[Dict, List[peewee.ModelSelect]]:
    if asdict:
        query = (model.UserModel.select().dicts())

    else:
        query = model.UserModel.select()

    return query


def get_user_from_id(playerid: str) -> peewee.ModelSelect:
    return model.UserModel.get(model.UserModel.playerid == playerid)


def get_all_match() -> peewee.ModelSelect:
    return model.MatchModel.select()


def modify_user(playerid: str, newname: str):
    user = model.UserModel.get_or_none(model.UserModel.playerid == playerid)
    if user is None:
        return False

    if user.name != newname:
        user.name = newname
        user.save(only=user.dirty_fields)

    return True


def link_user(playerid_alt: str, playerid_root: str):
    user = model.UserModel.get_or_none(model.UserModel.playerid == playerid_alt)
    if user is None:
        return False

    if user.root is not None:
        user.root = None

    else:
        root_user = model.UserModel.get_or_none(model.UserModel.playerid == playerid_root)
        if root_user is None:
            return False

        user.root = root_user

    user.save(only=user.dirty_fields)
    return True


def ban_user(playerid: str):
    user = model.UserModel.get_or_none(model.UserModel.playerid == playerid)
    if user is None:
        return False

    user.is_banned = not user.is_banned
    user.save(only=user.dirty_fields)
    return True


def is_match_exists(uuid: str):
    return model.MatchModel.select().where(model.MatchModel.uuid == uuid).exists()


def is_user_exists(playerid):
    return model.UserModel.select().where(model.UserModel.playerid == playerid).exists()
