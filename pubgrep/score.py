# coding: utf-8

"""
Score Functionality.
"""

import math

class ScoreAdapter:
    def __init__(self, config):
        self._conf = config

        self.available_func = {
            'linear': self.linear_score,
            'quad': self.quad_in_score,
            'cubic': self.cubic_in_score,
            'sine': self.sine_in_score,
        }

        self.most = 1000
        self.duration = 100
        self.bias = config['SCORE'].getint('bias', 0)
        self.killscore = config['SCORE'].getint('killscore', 50)
        self.function = config['SCORE'].get('formula', 'linear')

    def refreshsettings(self):
        self.most = 1000
        self.duration = 100
        self.bias = self._conf['SCORE'].getint('bias', 0)
        self.killscore = self._conf['SCORE'].getint('killscore', 50)
        self.function = self.conf['SCORE'].get('formula', 'linear')

    @property
    def function(self):
        return self._function
    
    @function.setter
    def function(self, func_name: str):
        if not isinstance(func_name, str):
            raise ValueError('Value should be a str.')

        if func_name not in self.available_func:
            self._function = self.linear_score

        else:
            self._function = self.available_func[func_name]

    def calculate_score():
        pass

    def __call__(self, rank, kill):
        return self._function(101 - rank) + (kill * self.killscore)

    def linear_score(self, num):
        return (num * self.most / self.duration + self.bias)

    def quad_in_score(self, num):
        df = num / self.duration
        return self.most * df * df + self.bias

    def cubic_in_score(self, num):
        df = num / self.duration
        return self.most * df * df * df + self.bias

    def sine_in_score(self, num):
        return -self.most * math.cos(num / self.duration * (math.pi / 2)) + self.most + self.bias

