# coding: utf-8

import requests
import json

MATCH_URL = 'https://api.pubg.com/shards/steam/matches/'
HEADER = {'Accept': "application/vnd.api+json"}

def send_request(url):
    try:
        req = requests.get(url, headers=HEADER)
    
    except Exception as e:
        return False

    if req.status_code != 200:
        return False

    return json.loads(req.content.decode('utf-8'))

def fetch_telemetry(match):
    asset = False
    for data in match.get('included', []):
        dtype = data.get('type')
        if dtype == 'asset':
            asset = data
            break

        else:
            continue

    else:
        return False

    telem_url = asset['attributes'].get('URL', False)
    if not telem_url:
        return False
        
    return send_request(telem_url)
