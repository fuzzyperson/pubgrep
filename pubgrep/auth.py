# coding: utf-8

import os
import bcrypt
import hashlib

PSWD = hashlib.sha256(os.urandom(32)).hexdigest()
password = bcrypt.hashpw(
     PSWD.encode('utf-8'),
     bcrypt.gensalt(12, prefix=b'2a'))

def check_password(input_password):
    return bcrypt.checkpw(input_password.encode(), password)

def generate_token():
    return hashlib.sha256(os.urandom(32)).hexdigest()

